/** create by zhangpu date:2015年3月11日 */
package com.acooly.module.openapi.client.provider.alipay.domain;

import com.acooly.module.openapi.client.provider.alipay.support.AlipayAlias;

import lombok.Getter;
import lombok.Setter;

/** @author zhangpu */
@Getter
@Setter
public class AlipayNotify extends AlipayResponse {

	/** 返回状态码 */
	@AlipayAlias(value = "trade_status")
	private String tradeStatus;
}
