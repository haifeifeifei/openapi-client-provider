package com.acooly.module.openapi.client.provider.allinpay.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 18:41
 * @Description:
 */
@Getter
@Setter
@ToString
@XStreamAlias("QTDETAIL")
public class AllinpayTradeQueryResponseDetail implements Serializable {

    /**
     * 交易批次号
     * 也就是原请求交易中的REQ_SN的值
     */
    @Size(max = 40)
    @XStreamAlias("BATCHID")
    private String batchId;

    /**
     * 记录序号
     * 也就是原请求交易中的SN的值
     */
    @Size(max = 4)
    @XStreamAlias("SN")
    private String sn;

    /**
     * 交易方向
     * 0 付 1收
     */
    @Size(max = 1)
    @XStreamAlias("TRXDIR")
    private String trxdir;

    /**
     * 清算日期
     * yyyyMMdd
     */
    @Size(max = 8)
    @XStreamAlias("SETTDAY")
    private String settDay;

    /**
     * 完成时间
     * yyyyMMddHHmmss
     */
    @Size(max = 14)
    @NotBlank
    @XStreamAlias("FINTIME")
    private String finishTime;

    /**
     * 提交时间
     * yyyyMMddHHmmss
     */
    @Size(max = 14)
    @XStreamAlias("SUBMITTIME")
    private String submitTime;

    /**
     * 账号
     * 只返回卡号后4位
     */
    @Size(max = 32)
    @XStreamAlias("ACCOUNT_NO")
    private String accountNo;

    /**
     * 账号名
     */
    @Size(max = 60)
    @XStreamAlias("ACCOUNT_NAME")
    private String accountName;

    /**
     * 金额
     */
    @Size(max = 12)
    @XStreamAlias("AMOUNT")
    private String amount;

    /**
     * 自定义用户号
     * 原代收付请求报文中的CUST_USERID字段
     */
    @Size(max = 128)
    @XStreamAlias("CUST_USERID")
    private String custUserId;

    /**
     * 备注
     */
    @Size(max = 50)
    @XStreamAlias("REMARK")
    private String remark;

    /**
     * 交易附言
     */
    @Size(max = 140)
    @XStreamAlias("SUMMARY")
    private String summary;

    /**
     * 返回码
     * 0000处理成功，其他参考参考附录B6
     */
    @Size(max = 15)
    @XStreamAlias("RET_CODE")
    private String retCode;

    /**
     * 错误文本
     */
    @Size(max = 256)
    @XStreamAlias("ERR_MSG")
    private String errMsg;
}
