package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/29 16:24
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.BATCH_DEDUCT_SIGLE_QUERY,type = ApiMessageType.Request)
public class BaoFuBatchDeductSigleQueryRequest extends BaoFuRequest{


    /**
     * 商户流水号 8-20 位字母和数字，每次请求都不可重复
     *
     */
    @BaoFuAlias(value = "trans_serial_no",request = false)
    private String transSerialNo = Ids.Did.getInstance().getId(20);

    /**
     * 原批量代扣子订单号
     *
     */
    @BaoFuAlias(value = "trans_id",request = false)
    private String transId;
}
