package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuResponse;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuBatchDeductSigleQueryResInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/29 16:24
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.BATCH_DEDUCT_SIGLE_QUERY,type = ApiMessageType.Response)
@XStreamAlias("result")
public class BaoFuBatchDeductSigleQueryResponse extends BaoFuResponse{

    /**
     * 代扣数据域
     * 实际信息 最多支持5000条
     */
    @XStreamAlias("order_info")
    private BaoFuBatchDeductSigleQueryResInfo baoFuBatchDeductSigleQueryResInfo;
}
