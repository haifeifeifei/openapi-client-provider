package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/26 13:56
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.REFUND,type = ApiMessageType.Request)
public class BaoFuRefundRequest extends BaoFuRequest {
    /**
     * 取值
     *1:宝付收银台
     *2:认证支付、代扣、快捷支付
     *3:微信支付
     *5:支付宝支付
     */
    @NotBlank
    @BaoFuAlias(value = "refund_type", request = false)
    private String refundType;

    /**
     * 原商户订单号
     * 原商户发起的支付订单号
     */
    @NotBlank
    @BaoFuAlias(value = "trans_id", request = false)
    private String transId;

    /**
     * 退款商户订单号
     * 退款时商户端生成的订单号
     */
    @NotBlank
    @BaoFuAlias(value = "refund_order_no", request = false)
    private String refundOrderNo;

    /**
     * 退款商户流水号
     * 每次发起退款的流水不能重复
     */
    @NotBlank
    @BaoFuAlias(value = "trans_serial_no", request = false)
    private String transSerialNo;

    /**
     * 退款原因
     */
    @NotBlank
    @BaoFuAlias(value = "refund_reason", request = false)
    private String refundReason;

    /**
     * 退款金额
     * 单位：分
     */
    @NotBlank
    @BaoFuAlias(value = "refund_amt", request = false)
    private String refundAmt;


    /**
     * 退款发起时间
     * 14 位定长。
     * 格式：年年年年月月日日时时分分秒秒
     */
    @NotBlank
    @BaoFuAlias(value = "refund_time", request = false)
    private String refundTime;

    /**
     * 附加字段
     * 长度不超过 128 位
     */
    @BaoFuAlias(value = "additional_info", request = false)
    private String additionalInfo;

    /**
     * 请求方保留域
     */
    @BaoFuAlias(value = "req_reserved", request = false)
    private String reqReserved;

    /**
     * 服务器通知商户地址
     * 扣款成功或者失败通知商户地址
     */
    @NotBlank
    @BaoFuAlias(value = "notice_url", request = false)
    private String noticeUrl;

    /**
     * 风险控制参数
     */
    @BaoFuAlias(value = "risk_item", request = false)
    private String riskItem;

}


































