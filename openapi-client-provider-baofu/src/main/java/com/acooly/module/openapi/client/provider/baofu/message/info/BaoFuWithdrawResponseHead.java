package com.acooly.module.openapi.client.provider.baofu.message.info;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/2/2 14:33
 */
@Getter
@Setter
@XStreamAlias("trans_head")
public class BaoFuWithdrawResponseHead implements Serializable{


    /**
     * 响应码元素返回交易处理状态码
     */
    @XStreamAlias("return_code")
    private String returnCode;

    /**
     * 响应信息元素交易处理状态中文信息
     */
    @XStreamAlias("return_msg")
    private String returnMsg;
}
