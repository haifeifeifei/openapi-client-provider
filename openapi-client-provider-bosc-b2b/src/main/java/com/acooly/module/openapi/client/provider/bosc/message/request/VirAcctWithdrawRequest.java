package com.acooly.module.openapi.client.provider.bosc.message.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequestDomain;

@Getter
@Setter
public class VirAcctWithdrawRequest extends BoscRequestDomain {

	/**
	 * 虚账号（出账方）
	 */
	@NotBlank
	private String eAcctNo;

	/**
	 * 转账金额
	 */
	@MoneyConstraint(min = 1L)
	private Money amount;

	/**
	 * 流水号
	 */
	@NotBlank
	private String channelFlowNo;

}
