上海银行互金银行存管
===

# 1.客户端

## 1.1.基础信息

|名称                         |值                      
|-----------------------------|-------------------------
|提供方(provider)              |上海银行（BOSC）
|产品(product)                 |P2P存管产品
|组件提供方名称                  |bosc
|组件名称                       |openapi-client-provider-bosc
|组件开发负责人                  |刘斌
|组件开发时间                    |2017-10-20
|主要使用项目或产品               |PCCB

## 1.2.简介

上海银行提供的P2P互金存管解决方案网关服务的专用接入组件。该组件负责所有的网关API接口开发接入，包括：用户开户和安全管理，投资，还款，分润，奖励等业务API，同时也封装了对账文件下载等功能。


# 2.网关服务

## 2.1.协议说明

|名称                         |值                      
|-----------------------------|-------------------------
|接口类型                       |同步，异步，跳转，文件下载
|通讯协议                       |http/https
|报文协议                       |http头传递签名及要输，http-body传输业务报文体。
|密钥                           |公私钥字符串（PEM格式）,由银行分配
|签名/验签                      |标准公私钥RSA签名算法，RSASinger
|报文加密                       |无

## 2.2.网关地址

### 2.2.1.联调地址

* 跳转接口网关：https://hubk.lanmaoly.com/bha-neo-app/lanmaotech/gateway
* 同异接口网关：https://hubk.lanmaoly.com/bha-neo-app/lanmaotech/service
* 下载接口网关：https://hubk.lanmaoly.com/bha-neo-app/lanmaotech/download

### 2.2.2.生产地址