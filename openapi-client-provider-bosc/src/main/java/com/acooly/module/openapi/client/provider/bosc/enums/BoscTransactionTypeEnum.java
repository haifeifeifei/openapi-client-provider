/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
public enum BoscTransactionTypeEnum implements Messageable {
	
	RECHARGE("RECHARGE","充值"),
	WITHDRAW("WITHDRAW","提现"),
	PRETRANSACTION("PRETRANSACTION","交易预处理"),
	TRANSACTION("TRANSACTION","交易确认"),
	FREEZE("FREEZE","冻结"),
	DEBENTURE_SALE("DEBENTURE_SALE","债权出让"),
	CANCEL_PRETRANSACTION("CANCEL_PRETRANSACTION","取消预处理"),
	UNFREEZE("UNFREEZE","解冻"),
	INTERCEPT_WITHDRAW("INTERCEPT_WITHDRAW","提现拦截"),
	GENERAL_FREEZE("GENERAL_FREEZE","通用冻结"),
	UPDATE_BANKCARD_AUDIT("UPDATE_BANKCARD_AUDIT","换卡记录"),
	;
	
	private final String code;
	private final String message;
	
	private BoscTransactionTypeEnum (String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String code() {
		return code;
	}
	
	public String message() {
		return message;
	}
	
	public static Map<String, String> mapping() {
		Map<String, String> map = Maps.newLinkedHashMap();
		for (BoscTransactionTypeEnum type : values()) {
			map.put(type.getCode(), type.getMessage());
		}
		return map;
	}
	
	/**
	 * 通过枚举值码查找枚举值。
	 *
	 * @param code 查找枚举值的枚举值码。
	 * @return 枚举值码对应的枚举值。
	 * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
	 */
	public static BoscTransactionTypeEnum find(String code) {
		for (BoscTransactionTypeEnum status : values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		throw new IllegalArgumentException("BoscAuditStatusEnum not legal:" + code);
	}
	
	/**
	 * 获取全部枚举值。
	 *
	 * @return 全部枚举值。
	 */
	public static List<BoscTransactionTypeEnum> getAll() {
		List<BoscTransactionTypeEnum> list = new ArrayList<BoscTransactionTypeEnum> ();
		for (BoscTransactionTypeEnum status : values()) {
			list.add(status);
		}
		return list;
	}
	
	/**
	 * 获取全部枚举值码。
	 *
	 * @return 全部枚举值码。
	 */
	public static List<String> getAllCode() {
		List<String> list = new ArrayList<String>();
		for (BoscTransactionTypeEnum status : values()) {
			list.add(status.code());
		}
		return list;
	}
		
	}
