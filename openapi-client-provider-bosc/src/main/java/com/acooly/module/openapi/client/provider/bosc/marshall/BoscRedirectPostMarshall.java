/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 16:11 创建
 */
package com.acooly.module.openapi.client.provider.bosc.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 跳转请求报文组装
 *
 * @author zhangpu 2017-09-24 16:11
 */
@Slf4j
@Component
public class BoscRedirectPostMarshall extends BoscMarshallSupport implements ApiMarshal<PostRedirect, BoscRequest> {

    @Override
    public PostRedirect marshal(BoscRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        postRedirect.setRedirectUrl(getProperties().getGatewayUrl());
        postRedirect.setFormDatas(doMarshall(source));
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }


}