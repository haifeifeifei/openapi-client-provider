package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;

@ApiMsgInfo(service = BoscServiceNameEnum.DOWNLOAD_CHECKFILE, type = ApiMessageType.Response)
public class DownloadCheckFileResponse extends BoscResponse {
	
	/**
	 * 下载好的文件名
	 */
	private String fileName;
	
	public String getFileName () {
		return fileName;
	}
	
	public void setFileName (String fileName) {
		this.fileName = fileName;
	}
}