package com.acooly.module.openapi.client.provider.bosc.message.fund.info;

import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizOriginEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscTradeTypeEnum;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 批量交易异步通知详情
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
public class NotifyDetailInfo extends BoscResponse{
	
	/**
	 * 业务流水号不能为空
	 */
	@NotBlank(message = "业务流水号不能为空")
	private String asyncRequestNo;
	
	/**
	 * 交易类型
	 */
	private BoscTradeTypeEnum bizType;
	/**
	 * 业务来源
	 */
	private BoscBizOriginEnum bizOrigin = BoscBizOriginEnum.DISPERSION;
	/**
	 * 交易创建时间
	 */
	@NotNull(message = "交易创建时间不能为空")
	private Date createTime;
	
	/**
	 * 交易完成时间
	 */
	@NotNull(message = "交易完成时间不能为空")
	private Date transactionTime;
	
	
	public String getAsyncRequestNo () {
		return asyncRequestNo;
	}
	
	public void setAsyncRequestNo (String asyncRequestNo) {
		this.asyncRequestNo = asyncRequestNo;
	}
	
	public BoscTradeTypeEnum getBizType () {
		return bizType;
	}
	
	public void setBizType (BoscTradeTypeEnum bizType) {
		this.bizType = bizType;
	}
	
	public BoscBizOriginEnum getBizOrigin () {
		return bizOrigin;
	}
	
	public void setBizOrigin (BoscBizOriginEnum bizOrigin) {
		this.bizOrigin = bizOrigin;
	}
	
	public Date getCreateTime () {
		return createTime;
	}
	
	public void setCreateTime (Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getTransactionTime () {
		return transactionTime;
	}
	
	public void setTransactionTime (Date transactionTime) {
		this.transactionTime = transactionTime;
	}
}
