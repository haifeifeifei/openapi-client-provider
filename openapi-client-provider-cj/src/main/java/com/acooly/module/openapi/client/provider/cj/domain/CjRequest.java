/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.cj.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu
 */
@Getter
@Setter
public class CjRequest extends CjApiMessage {

    /**
     * 报文交易代码 标识此请求为“单笔垫资付款业务”
     */
    private String trxCode;
    /**
     * 版本号
     */
    private String version;
    /**
     * 商户代码 标识商户的唯一ID，15位
     */
    private String mertid;
    /**
     * 交易请求号 数据格式：(15位)商户号 + (12位)yyMMddHHmmss时间戳 + (5位)循环递增序号 = (32位)唯一交易号；
     */
    private String reqSn;
    /**
     * 受理时间 代支付系统接收到交易请求时服务器时间； 对于交易的发起时间以此时间为准
     */
    private String timestamp;
    /**
     * 二级商户号
     */
    private String subMertid;

    /** 协议号 */
    private String protocolNo;

}
