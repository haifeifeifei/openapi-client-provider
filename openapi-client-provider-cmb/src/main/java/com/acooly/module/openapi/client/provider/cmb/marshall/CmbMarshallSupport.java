/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月4日
 *
 */
package com.acooly.module.openapi.client.provider.cmb.marshall;

import com.google.common.collect.Maps;

import com.acooly.core.utils.Reflections;
import com.acooly.core.utils.Strings;
import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.provider.cmb.CmbProperties;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbApiMessage;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbRequest;
import com.acooly.module.openapi.client.provider.cmb.utils.MapHelper;
import com.acooly.module.openapi.client.provider.cmb.utils.SignUtils;
import com.acooly.module.safety.Safes;
import com.acooly.module.safety.signature.SignTypeEnum;
import com.acooly.module.safety.signature.SignerFactory;
import com.acooly.module.safety.support.CodecEnum;
import com.acooly.module.safety.support.KeyPair;

import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

/**
 * @author zhangpu
 */
@Slf4j
public class CmbMarshallSupport {

    @Autowired
    protected CmbProperties cmbProperties;

    @Autowired
    protected SignerFactory signerFactory;

    private KeyPair keyPair;

    protected CmbProperties getProperties() {
        return cmbProperties;
    }

    protected String doMarshall(CmbRequest source) {
        doVerifyParam(source);



        Map<String, String> requestDataMap = getRequestDataMap(source);
        String signContent = SignUtils.getSignContent(requestDataMap);
        log.debug("待签字符串:{}", signContent);
        source.setSign(SignUtils.urlEncode(doSign(signContent)));



        return signContent+"&signature="+source.getSign();
    }

    /**
     * 获取代签map
     *
     * @param source
     * @return
     */
    protected Map<String, String> getRequestDataMap(CmbRequest source) {
        Map<String, String> requestData = Maps.newTreeMap();
        Set<Field> fields = Reflections.getFields(source.getClass());
        Object value = null;
        for (Field field : fields) {
            value = Reflections.getFieldValue(source, field.getName());
            if (value == null) {
                continue;
            }
            if (field.getType() != String.class) {
                requestData = MapHelper.toLinkedHashMap(value);
            }
        }
        return requestData;
    }

    /**
     * 验签
     *
     * @param signature
     * @param plain
     * @return
     */
    protected void doVerifySign(String signature, String plain, String partnerId) {
        Safes.getSigner(SignTypeEnum.Rsa.name ()).verify(plain, getKeyPair(), signature);
    }


    /**
     * 校验参数
     *
     * @param source
     */
    protected void doVerifyParam(CmbApiMessage source) {
        try {
            source.doCheck();
            Validators.assertJSR303(source);
        } catch (Exception e) {
            throw new ApiClientException(e.getMessage());
        }
    }


    protected void beforeMarshall(CmbApiMessage message) {
        if(Strings.isBlank(message.getPartner_id())) {
            message.setPartner_id(getProperties().getPartnerId());
        }
    }


    protected String doSign(String waitForSign) {
        return Safes.getSigner(SignTypeEnum.Rsa.name()).sign(waitForSign, getKeyPair());
    }

    protected KeyPair getKeyPair() {
        if (keyPair == null) {
            synchronized (this) {
                if (keyPair == null) {
                    keyPair = new KeyPair(cmbProperties.getPublicKey(),
                            cmbProperties.getPrivateKey());
                    keyPair.loadKeys();
                }
            }
        }
        keyPair.setSignatureCodec(CodecEnum.HEX);
        keyPair.setSignatureAlgo("SHA256withRSA");
        return keyPair;
    }


}
