/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.fudian.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author zhangpu
 * @date 2018-1-23
 */
@Slf4j
@Component
public class FudianResponseUnmarshall extends FudianAbstractMarshall implements ApiUnmarshal<FudianResponse, String> {


    @SuppressWarnings("unchecked")
    @Override
    public FudianResponse unmarshal(String message, String serviceName) {
        return doUnMarshall(message, serviceName, false);
    }


}
