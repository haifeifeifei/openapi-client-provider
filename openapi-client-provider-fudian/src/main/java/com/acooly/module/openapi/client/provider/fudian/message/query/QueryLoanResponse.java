/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:32:16 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.query;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:32:16
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.QUERY_LOAN ,type = ApiMessageType.Response)
public class QueryLoanResponse extends FudianResponse {

    /**
     * 募集金额
     * 标的发标金额，若截标则是截标后的金额
     */
    @NotEmpty
    @Length(max=20)
    private String amount;

    /**
     * 标的账户资金
     * 标的账户目前账户余额
     */
    @NotEmpty
    @Length(max=20)
    private String balance;

    /**
     * 标的号
     * 标的号，由存管系统生成并确保唯一性.
     */
    @NotEmpty
    @Length(max=32)
    private String loanTxNo;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 状态
     * 标的状态：0：开标、1：投标中、2：满标放款成功、3：还款中、4：还款完成5撤销成功
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String status;
}