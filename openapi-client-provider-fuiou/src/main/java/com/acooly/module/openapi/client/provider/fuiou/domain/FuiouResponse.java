/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.domain;

import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Fuiou 响应报文基类
 * 
 * 
 * @author zhangpu
 */
@XStreamAlias("plain")
public class FuiouResponse extends FuiouApiMessage {

	@XStreamAlias("resp_code")
	@FuiouAlias("resp_code")
	private String respCode;

	private String message;

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
