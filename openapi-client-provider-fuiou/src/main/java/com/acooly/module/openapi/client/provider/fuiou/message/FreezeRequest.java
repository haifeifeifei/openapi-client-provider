package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouRequest;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 单独冻结 请求报文
 * 
 * @author liuyuxiang
 */
public class FreezeRequest extends FuiouRequest {

	/** 用户名 */
	@NotEmpty
	@Size(min = 1, max = 20)
	@FuiouAlias("cust_no")
	private String custNo;
	
	/** 冻结金额 */
	@NotEmpty
	@Size(min = 1, max = 10)
	@FuiouAlias("amt")
	private String amt;
	
	/** 备注 */
	@Size(min = 0, max = 60)
	@FuiouAlias("rem")
	private String rem;

	public FreezeRequest(){}
	
	public FreezeRequest(String custNo, String amt, String rem) {
		this.custNo = custNo;
		this.amt = amt;
		this.rem = rem;
	}

	public String getCustNo() {
		return this.custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getAmt() {
		return this.amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getRem() {
		return this.rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}

}
