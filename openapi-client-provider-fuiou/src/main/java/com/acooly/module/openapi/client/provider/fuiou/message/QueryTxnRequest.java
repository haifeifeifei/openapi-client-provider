/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月5日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouRequest;
import com.acooly.module.openapi.client.provider.fuiou.enums.FuiouBusiTpEnums;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 交易查询 请求报文
 * 
 * @author liuyuxiang
 */
public class QueryTxnRequest extends FuiouRequest {

	/** 交易类型 */
	@NotEmpty
	@FuiouAlias("busi_tp")
	private FuiouBusiTpEnums busiTp;

	/** 起始时间 (yyyyMMdd)*/
	@NotEmpty
	@Size(min = 8, max = 8)
	@FuiouAlias("start_day")
	private String startDay;
	
	/** 截止时间(yyyyMMdd) */
	@NotEmpty
	@Size(min = 8, max = 8)
	@FuiouAlias("end_day")
	private String endDay;
	
	/** 交易流水 */
	@Size(min = 0, max = 30)
	@FuiouAlias("txn_ssn")
	private String txnSsn;
	
	/** 交易用户 */
	@Size(min = 0, max = 60)
	@FuiouAlias("cust_no")
	private String custNo;

	/** 交易状态 */
	@Size(min = 0, max = 1)
	@FuiouAlias("txn_st")
	private String txnSt;
	
	/** 交易备注 */
	@Size(min = 0, max = 200)
	@FuiouAlias("remark")
	private String remark;
	
	/** 页码 */
	@FuiouAlias("page_no")
	private String pageNo;
	
	/** 每页条数 10-100 */
	@Size(min = 0, max = 3)
	@FuiouAlias("page_size")
	private String pageSize;

	
	public QueryTxnRequest() {
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public FuiouBusiTpEnums getBusiTp() {
		return this.busiTp;
	}

	public void setBusiTp(FuiouBusiTpEnums busiTp) {
		this.busiTp = busiTp;
	}

	public String getStartDay() {
		return this.startDay;
	}

	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}

	public String getEndDay() {
		return this.endDay;
	}

	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}

	public String getTxnSsn() {
		return this.txnSsn;
	}

	public void setTxnSsn(String txnSsn) {
		this.txnSsn = txnSsn;
	}

	public String getTxnSt() {
		return this.txnSt;
	}

	public void setTxnSt(String txnSt) {
		this.txnSt = txnSt;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPageNo() {
		return this.pageNo;
	}

	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}

	public String getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

}
