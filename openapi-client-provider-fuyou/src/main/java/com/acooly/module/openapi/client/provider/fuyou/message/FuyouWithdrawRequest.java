package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_WITHDRAW,type = ApiMessageType.Request)
@XStreamAlias("payforreq")
public class FuyouWithdrawRequest extends FuyouRequest{

    /**
     * 版本号
     * 1.0
     */
    @XStreamAlias("ver")
    @Size(max = 4)
    private String ver = "1.00";

    /**
     * 请求日期
     * yyyyMMdd
     */
    @XStreamAlias("merdt")
    @Size(max = 4)
    private String merdt = Dates.format(new Date(),"yyyyMMdd");

    /**
     * 请求流水
     * 数字串，当天必须唯一
     */
    @XStreamAlias("orderno")
    @NotBlank
    @Size(max = 30)
    private String orderNo = Ids.oid();

    /**
     * 总行代码
     * 参见总行代码表
     */
    @XStreamAlias("bankno")
    @NotBlank
    @Size(max = 4)
    private String bankNo = "1000";

    /**
     * 城市代码
     * 参见地市代码表
     */
    @XStreamAlias("cityno")
    @NotBlank
    @Size(max = 4)
    private String cityNo;

    /**
     * 支行名称
     * 可不填，如果填写，一定要填对，否则影响交易；
     * 但对公户、城商行、农商行、信用社必须填支行，
     * 且需正确的支行信息
     */
    @XStreamAlias("branchnm")
    @Size(max = 100)
    private String branchnm;

    /**
     * 账号
     * 用户账号
     */
    @XStreamAlias("accntno")
    @NotBlank
    @Size(max = 28)
    private String accntNo;

    /**
     * 账户名称
     * 用户账户名称
     */
    @XStreamAlias("accntnm")
    @NotBlank
    @Size(max = 30)
    private String accntNm;

    /**
     * 金额
     * 客单位：分（单笔最少 3 元）
     */
    @XStreamAlias("amt")
    @NotBlank
    @Size(max = 12)
    private String amt;

    /**
     * 企业流水号
     * 填写后，系统体现在交易查询中
     */
    @XStreamAlias("entseq")
    @Size(max = 32)
    private String entseq;

    /**
     * 备注
     * 填写后，系统体现在交易查询中
     */
    @XStreamAlias("memo")
    @Size(max = 64)
    private String memo;

    /**
     * 手机号
     * 短信通知时使用
     */
    @XStreamAlias("mobile")
    @Size(max = 64)
    private String mobile;

    /**
     * 是否返回交易状态类别
     * 值为 1，其他值或不填则认为不需要返回交易状态类别且响应参数无状态类别节点
     */
    @XStreamAlias("addDesc")
    @Size(max = 64)
    private String addDesc = "1";

}
