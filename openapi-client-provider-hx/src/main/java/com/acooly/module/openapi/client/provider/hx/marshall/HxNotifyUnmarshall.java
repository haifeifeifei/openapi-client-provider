///**
// * create by zhangpu
// * date:2015年3月12日
// */
//package com.acooly.module.openapi.client.provider.hx.marshall;
//
//import com.acooly.core.utils.Reflections;
//import com.acooly.core.utils.Strings;
//import com.acooly.module.openapi.client.api.anotation.ApiItem;
//import com.acooly.module.openapi.client.api.exception.ApiClientException;
//import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
//import com.acooly.module.openapi.client.api.message.MessageFactory;
//import com.acooly.module.openapi.client.provider.hx.HxConstants;
//import com.acooly.module.openapi.client.provider.hx.domain.HxNotify;
//import com.acooly.module.openapi.client.provider.hx.enums.HxServiceEnum;
//import com.acooly.module.openapi.client.provider.hx.partner.HxPartnerIdLoadManager;
//import com.acooly.module.openapi.client.provider.hx.support.HxRespCodes;
//import com.acooly.module.openapi.client.provider.hx.utils.SignUtils;
//import com.acooly.module.safety.Safes;
//import com.acooly.module.safety.key.KeyLoadManager;
//import com.acooly.module.safety.signature.SignTypeEnum;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.lang.reflect.Field;
//import java.util.Map;
//import java.util.Set;
//
//import javax.annotation.Resource;
//
///**
// * @author zhangpu
// */
//@Service
//public class HxNotifyUnmarshall extends HxMarshallSupport
//        implements ApiUnmarshal<HxNotify, Map<String, String>> {
//    private static final Logger logger = LoggerFactory.getLogger(HxNotifyUnmarshall.class);
//    @Resource(name = "weBankMessageFactory")
//    private MessageFactory messageFactory;
//
//    @Resource(name = "weBankPartnerIdLoadManager")
//    private HxPartnerIdLoadManager partnerIdLoadManager;
//
//    @Autowired
//    private KeyLoadManager keyStoreLoadManager;
//
//    @SuppressWarnings("unchecked")
//    @Override
//    public HxNotify unmarshal(Map<String, String> message, String serviceName) {
//        try {
//            logger.info("异步通知{}", message);
//            String signature = message.get(HxConstants.SIGN);
//            message.remove(HxConstants.SIGN);
//            String plain = SignUtils.getSignContent(message);
//            //验签
//            Safes.getSigner(SignTypeEnum.Rsa.name ()).verify(plain, getKeyPair(), signature);
//            return doUnmarshall(message, serviceName);
//        } catch (Exception e) {
//            throw new ApiClientException("异步通知 解析失败:" + e.getMessage());
//        }
//
//    }
//
//    protected HxNotify doUnmarshall(Map<String, String> message, String serviceName) {
//        HxNotify notify = (HxNotify) messageFactory.getNotify(serviceName);
//        Set<Field> fields = Reflections.getFields(notify.getClass());
//        String key = null;
//        for (Field field : fields) {
//            ApiItem apiItem = field.getAnnotation(ApiItem.class);
//            if (apiItem != null && Strings.isNotBlank(apiItem.value())) {
//                key = apiItem.value();
//            }else {
//                key = field.getName();
//            }
//            Reflections.invokeSetter(notify, field.getName(), message.get(key));
//        }
//        notify.setBizType(HxServiceEnum.find(serviceName).getCode());
//        if (Strings.isNotBlank(notify.getCode())) {
//            notify.setMessage(HxRespCodes.getMessage(notify.getCode()));
//        }
//        return notify;
//    }
//
//    public void setMessageFactory(MessageFactory messageFactory) {
//        this.messageFactory = messageFactory;
//    }
//
//}
