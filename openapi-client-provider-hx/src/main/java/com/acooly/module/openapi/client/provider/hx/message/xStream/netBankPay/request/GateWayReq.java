package com.acooly.module.openapi.client.provider.hx.message.xStream.netBankPay.request;

import com.acooly.module.openapi.client.provider.hx.message.xStream.common.ReqHead;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/3/2 17:50.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("GateWayReq")
public class GateWayReq {

    @XStreamAlias("head")
    private ReqHead head;

    @XStreamAlias("body")
    private ReqNetBankBody body;
}
