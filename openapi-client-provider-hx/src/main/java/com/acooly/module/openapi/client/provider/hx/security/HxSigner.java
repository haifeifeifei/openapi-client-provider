/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月4日
 *
 */
package com.acooly.module.openapi.client.provider.hx.security;

import com.acooly.module.openapi.client.provider.hx.HxConstants;
import com.acooly.module.openapi.client.provider.hx.HxProperties;
import com.acooly.module.safety.signature.Signer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author zhangpu
 */
@Service
public class HxSigner implements Signer<String, HxProperties> {

    private static final Logger logger = LoggerFactory.getLogger(HxSigner.class);

    /**** copy from fuiou sdk ********/

    @Override
    public String sign(String t, HxProperties key) {
        logger.debug("待签字符串:{}", t);
        String result = null;
        try {

            logger.debug("签名成功:{}", result);
        } catch (Exception e) {
            logger.error("签名失败. getWaitToSigin:{}，错误:{}", t, e.getMessage());
        }
        return result;
    }

    @Override
    public void verify(String plain, HxProperties key, String sign) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getSinType() {
        return HxConstants.SIGNER_KEY;
    }
}
