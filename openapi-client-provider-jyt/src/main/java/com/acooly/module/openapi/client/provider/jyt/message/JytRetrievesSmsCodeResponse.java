package com.acooly.module.openapi.client.provider.jyt.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.jyt.domain.JytApiMsgInfo;
import com.acooly.module.openapi.client.provider.jyt.domain.JytResponse;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytRetrievesSmsCodeResponseBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/8 16:23
 */
@Getter
@Setter
@JytApiMsgInfo(service = JytServiceEnum.RETRIEVES_SMS_CODE,type = ApiMessageType.Response)
@XStreamAlias("message")
public class JytRetrievesSmsCodeResponse extends JytResponse {

    /**
     * 响应报文体
     */
    @XStreamAlias("body")
    private JytRetrievesSmsCodeResponseBody retrievesSmsCodeResponseBody;
}
