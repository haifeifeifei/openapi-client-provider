package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * @author zhike 2018/6/14 15:10
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytCheckCardConfirmRequestBody implements Serializable {

    /**
     * 绑卡编号
     */
    @NotBlank
    @XStreamAlias("bind_order_id")
    private String bindOrderNo;

    /**
     * 短息验证码
     */
    @NotBlank
    @XStreamAlias("verify_code")
    private String smsCode;
}
