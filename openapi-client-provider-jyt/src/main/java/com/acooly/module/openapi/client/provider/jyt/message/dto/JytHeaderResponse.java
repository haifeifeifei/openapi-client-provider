package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/7 23:34
 */
@Getter
@Setter
@XStreamAlias("head")
public class JytHeaderResponse extends JytHeader {

  /** 响应码 返回码 成功：S0000000 错误：返回具体的响应码。平台返回的错误码为8位，详情参看“接口响应码说明”章节 */
    @XStreamAlias("resp_code")
    private String respCode;

    /** 响应码描述 中文描述 */
    @XStreamAlias("resp_desc")
    private String respDesc;
}
