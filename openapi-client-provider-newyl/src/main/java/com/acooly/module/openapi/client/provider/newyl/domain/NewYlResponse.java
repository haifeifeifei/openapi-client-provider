/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.newyl.domain;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.io.naming.NoNameCoder;
import com.thoughtworks.xstream.io.xml.DomDriver;
import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 *
 * @author zhangpu
 */
@Getter
@Setter
public class NewYlResponse extends NewYlApiMessage {

	@XStreamOmitField
	private XStream xStream;

	/**
	 * 数据类型 xml/json
	 *
	 * @param
	 */
	public NewYlResponse() {
		xStream = new XStream(new DomDriver("GBK", new NoNameCoder()));
		// 启用Annotation
		xStream.autodetectAnnotations(true);
	}

	public Object str2Obj(String xml,Object root){
		xStream.alias("GZZF", root.getClass());
		return xStream.fromXML(xml,root);
	}

}
