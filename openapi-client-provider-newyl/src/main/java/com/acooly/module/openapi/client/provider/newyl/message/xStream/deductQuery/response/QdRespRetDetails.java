package com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.List;

/**
 * @author fufeng 2018/1/26 15:29.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("RET_DETAILS")
public class QdRespRetDetails {
    @XStreamImplicit(itemFieldName="RET_DETAIL")
    private List<QdRespRetDetail> retDetail;

}
