/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.shengpay.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
public enum ShengpayBankCodeEnum implements Messageable {
    BANK_CODE_ABC("农业银行","ABC"),
    BANK_CODE_ICBC("工商银行","ICBC"),
    BANK_CODE_BOC("中国银行","BOC"),
    BANK_CODE_HXB("华夏银行","HXB"),
    BANK_CODE_SZPAB("平安银行","SZPAB"),
    BANK_CODE_CCB("建设银行","CCB"),
    BANK_CODE_CEB("光大银行","CEB"),
    BANK_CODE_CITIC("中信银行","CITIC"),
    BANK_CODE_CMB("招商银行","CMB"),
    BANK_CODE_CIB("兴业银行","CIB"),
    BANK_CODE_COMM("交通银行","COMM"),
    BANK_CODE_SPDB("浦发银行","SPDB"),
    BANK_CODE_CMBC("民生银行","CMBC"),
    BANK_CODE_BOS("上海银行","BOS"),
    BANK_CODE_SDB("深圳发展银行","SDB"),
    ;

    private final String code;
    private final String message;

    private ShengpayBankCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (ShengpayBankCodeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static ShengpayBankCodeEnum find(String code) {
        for (ShengpayBankCodeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("FudianAuditStatusEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<ShengpayBankCodeEnum> getAll() {
        List<ShengpayBankCodeEnum> list = new ArrayList<ShengpayBankCodeEnum>();
        for (ShengpayBankCodeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (ShengpayBankCodeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
