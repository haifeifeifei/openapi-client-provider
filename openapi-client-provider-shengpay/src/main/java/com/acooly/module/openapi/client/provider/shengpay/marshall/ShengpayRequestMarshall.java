/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-24 16:11 创建
 */
package com.acooly.module.openapi.client.provider.shengpay.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 同步请求报文组装
 *
 * @author zhike 2018-1-23 16:11
 */
@Slf4j
@Component
public class ShengpayRequestMarshall extends ShengpayAbstractMarshall implements ApiMarshal<Map<String, String>, ShengpayRequest> {

    @Override
    public Map<String, String> marshal(ShengpayRequest source) {
        return doMarshall(source);
    }


}
