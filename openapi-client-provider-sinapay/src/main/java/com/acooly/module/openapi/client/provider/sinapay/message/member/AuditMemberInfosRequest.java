/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 请求审核企业会员资质 请求报文
 * 
 * @author zhike
 */
@SinapayApiMsg(service = SinapayServiceNameEnum.AUDIT_MEMBER_INFOS, type = ApiMessageType.Request)
@Getter
@Setter
public class AuditMemberInfosRequest extends SinapayRequest {

	/**
	 * 请求审核订单号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "audit_order_no")
	private String auditOrderNo = Ids.oid();

	/**
	 * 用户标识信息
	 *
	 * 商户系统用户id(字母或数字)
	 */
	@NotEmpty
	@Size(max = 50)
	@ApiItem(value = "identity_id")
	private String identityId;

	/**
	 * 用户标识类型
	 *
	 * ID的类型，目前只包括UID
	 */
	@NotEmpty
	@Size(max = 16)
	@ApiItem(value = "identity_type")
	private String identityType = "UID";

	/**
	 * 会员类型
	 *
	 * 会员类型，详见附录，默认企业，且只支持企业
	 */
	@Size(max = 1)
	@ApiItem(value = "member_type")
	private String memberType;

	/**
	 * 公司名称
	 *
	 * 公司名称全称，以便审核通过
	 */
	@NotEmpty
	@Size(max = 90)
	@ApiItem(value = "company_name")
	private String companyName;

	/**
	 * 企业网址
	 *
	 * 企业网址
	 */
	@Size(max = 90)
	@ApiItem(value = "website")
	private String website;

	/**
	 * 企业地址
	 *
	 * 企业地址
	 */
	@NotEmpty
	@Size(max = 90)
	@ApiItem(value = "address")
	private String address;

	/**
	 * 执照号
	 *
	 * 密文，使用新浪支付RSA公钥加密。明文长度：50
	 */
	@NotEmpty
	@Size(max = 50)
	@ApiItem(value = "license_no", securet = true)
	private String licenseNo;

	/**
	 * 营业执照所在地
	 *
	 * 营业执照所在地
	 */
	@NotEmpty
	@Size(max = 50)
	@ApiItem(value = "license_address")
	private String licenseAddress;

	/**
	 * 执照过期日（营业期限）
	 *
	 * 格式为“YYYYMMDD”
	 */
	@NotEmpty
	@Size(max = 10)
	@ApiItem(value = "license_expire_date")
	private String licenseExpireDate;

	/**
	 * 营业范围
	 *
	 * 营业范围
	 */
	@NotEmpty
	@Size(max = 256)
	@ApiItem(value = "business_scope")
	private String businessScope;

	/**
	 * 联系电话
	 *
	 * 密文，使用新浪支付RSA公钥加密。明文长度：50
	 */
	@NotEmpty
	@Size(max = 20)
	@ApiItem(value = "telephone", securet = true)
	private String telephone;

	/**
	 * 联系Email
	 *
	 * 密文，使用新浪支付RSA公钥加密。明文长度：50
	 */
	@NotEmpty
	@Size(max = 50)
	@ApiItem(value = "email", securet = true)
	private String email;

	/**
	 * 组织机构代码
	 *
	 * 密文，使用新浪支付RSA公钥加密。明文长度：50
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "organization_no", securet = true)
	private String organizationNo;

	/**
	 * 企业简介
	 *
	 * 企业简介
	 */
	@NotEmpty
	@Size(max = 512)
	@ApiItem(value = "summary")
	private String summary;

	/**
	 * 企业法人
	 *
	 * 密文，使用新浪支付RSA公钥加密。明文长度：50
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "legal_person", securet = true)
	private String legalPerson;

	/**
	 * 法人证件号码
	 *
	 * 密文，使用新浪支付RSA公钥加密。明文长度：50
	 */
	@NotEmpty
	@Size(max = 18)
	@ApiItem(value = "cert_no", securet = true)
	private String certNo;

	/**
	 * 证件类型
	 *
	 * 见附录，目前只支持身份证
	 */
	@NotEmpty
	@Size(max = 18)
	@ApiItem(value = "cert_type")
	private String certType = "IC";

	/**
	 * 法人证件生效时间
	 * 证件有效期开始时间
	 */
	@NotEmpty
	@Size(max = 8)
	@ApiItem(value = "cert_effect_date")
	private String certEffectDate;

	/**
	 * 法人证件失效时间
	 * 证件有效期失效时间，如果是长期有效则传“forever”
	 * 20151002
	 */
	@NotEmpty
	@Size(max = 8)
	@ApiItem(value = "cert_invalid_date")
	private String certInvalidDate;

	/**
	 * 法人手机号码
	 *
	 * 密文，使用新浪支付RSA公钥加密。明文长度：50
	 */
	@NotEmpty
	@Size(max = 20)
	@ApiItem(value = "legal_person_phone", securet = true)
	private String legalPersonPhone;

	/**
	 * 银行编号
	 *
	 * 见附录
	 */
	@Size(max = 10)
	@ApiItem(value = "bank_code")
	private String bankCode;

	/**
	 * 银行卡号
	 *
	 * 密文，使用新浪支付RSA公钥加密。明文长度：30
	 */
	@Size(max = 30)
	@ApiItem(value = "bank_account_no", securet = true)
	private String bankAccountNo;

	/**
	 * 卡类型
	 *
	 * 见附录 仅支持借记卡，默认为借记卡
	 */
	@Size(max = 10)
	@ApiItem(value = "card_type")
	private String cardType;

	/**
	 * 卡属性
	 *
	 * 见附录 仅支持对公，默认为对公
	 */
	@Size(max = 10)
	@ApiItem(value = "card_attribute")
	private String cardAttribute;

	/**
	 * 开户行省份
	 *
	 * 开户行省份
	 */
	@Size(max = 128)
	@ApiItem(value = "province")
	private String province;

	/**
	 * 开户行城市
	 *
	 * 开户行城市
	 */
	@Size(max = 128)
	@ApiItem(value = "city")
	private String city;

	/**
	 * 支行名称
	 *
	 * 银行支行名称
	 */
	@Size(max = 255)
	@ApiItem(value = "bank_branch")
	private String bankBranch;

	/**
	 * 文件名称
	 *
	 * 文件格式: *.zip 且文件名只能由数字或字母组成
	 */
	@Size(max = 32)
	@ApiItem(value = "fileName")
	private String fileName;

	/**
	 * 文件摘要
	 *
	 * 当fileName不为空时，该字段不可为空
	 */
	@Size(max = 32)
	@ApiItem(value = "digest")
	private String digest;

	/**
	 * 文件摘要算法
	 *
	 * 见附录，目前只支持MD5，默认为MD5
	 */
	@Size(max = 16)
	@ApiItem(value = "digestType")
	private String digestType = "MD5";
	
	/**
	 * 用户IP/运营商IP
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "client_ip")
	private String clientIp;
}
