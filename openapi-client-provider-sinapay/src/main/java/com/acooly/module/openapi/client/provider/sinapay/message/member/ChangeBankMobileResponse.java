package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRedirect;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 16:31
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CHANGE_BANK_MOBILE, type = ApiMessageType.Response)
public class ChangeBankMobileResponse extends SinapayResponse {

    /**
     *后续推进需要的参数
     * 修改推进时需要带上此参数，ticket有效期为15分钟，可以多次使用（最多5次）
     */
    @NotEmpty
    @Size(max = 100)
    @ApiItem(value = "ticket")
    private String ticket;

    /**
     *银行卡是否已认证
     * Y：已认证；N：未认证
     */
    @ApiItem(value = "is_verified")
    private String isVerified;
}
