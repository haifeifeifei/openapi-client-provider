/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 代付到卡 响应报文
 * 
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_SINGLE_HOSTING_PAY_TO_CARD_TRADE, type = ApiMessageType.Response)
public class CreateSingleHostingPayToCardTradeResponse extends SinapayResponse {
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "outer_trade_no")
	private String outerTradeNo;

	/**
	 * 交易状态
	 *
	 * 交易状态，详见附录“提现状态”
	 * <p>
	 * <li>SUCCESS：成功(系统会异步通知)
	 * <li>FAILED：失败(系统会异步通知)
	 * <li>PROCESSING：处理中(系统不会异步通知)
	 * <li>RETURNT_TICKET：退票(系统会异步通知)
	 * </p>
	 */
	@NotEmpty
	@Size(max = 20)
	@ApiItem(value = "withdraw_status")
	private String tradeStatus;
}
