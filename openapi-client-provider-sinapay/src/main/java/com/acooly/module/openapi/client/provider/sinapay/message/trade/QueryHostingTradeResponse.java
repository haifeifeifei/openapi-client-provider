/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.TradeItem;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 
 * 交易订单查询 响应报文
 * 
 * 
 * @author zhike
 */
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_HOSTING_TRADE, type = ApiMessageType.Response)
public class QueryHostingTradeResponse extends SinapayResponse {

	/**
	 * 页号
	 *
	 * 页号，从1开始，默认为1
	 */
	@Min(1)
	@ApiItem(value = "page_no")
	private int pageNo;

	/**
	 * 每页大小
	 *
	 * 每页记录数，默认20
	 */
	@Min(1)
	@ApiItem(value = "page_size")
	private int pageSize;

	@Size(max = 10)
	@ApiItem(value = "total_item")
	private int totalItem;

	@ApiItem("trade_list")
	private List<TradeItem> tradeList = Lists.newArrayList();

}
