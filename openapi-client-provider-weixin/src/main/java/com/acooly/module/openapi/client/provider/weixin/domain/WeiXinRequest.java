/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.weixin.domain;

import com.acooly.module.openapi.client.provider.weixin.support.WeixinAlias;

import lombok.Getter;
import lombok.Setter;

/** @author zhangpu */
@Getter
@Setter
public class WeixinRequest extends WeixinApiMessage {

	/**
	 * 商户订单号
	 */
	@WeixinAlias(value = "out_trade_no")
	private String outTradeNo;
}
