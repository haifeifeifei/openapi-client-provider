package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftResponse;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2017/11/28 21:01
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.PAY_ALIPAY_NATIVE, type = ApiMessageType.Response)
public class WftPayAlipayNativeResponse extends WftResponse {

    /**
     * --------------------------以下字段在 status 和 result_code 都为 0的时候有返回--------------------------------
     */

    /**
     * 二维码链接,商户可用此参数自定义去生成二维码后展示出来进行扫码支付
     */
    @WftAlias("code_url")
    private String codeUrl;

    /**
     * 二维码图片,此参数的值即是根据code_url生成的可以扫码支付的二维码图片地址
     */
    @WftAlias("code_img_url")
    private String codeImgUrl;
}
