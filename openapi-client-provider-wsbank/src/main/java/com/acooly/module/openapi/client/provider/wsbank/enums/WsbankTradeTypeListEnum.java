/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.wsbank.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 禁用支付方式。商户禁受理支付方式列表，多个用逗号隔开。
 *
 * @author weili
 * Date: 2018-05-30 15:12:06
 */
public enum WsbankTradeTypeListEnum implements Messageable {

//    NO_DEFINE("01", "不禁用"),
//    CREADIT("02", "信用卡"),
//    HBZF("03", "花呗（仅支付宝）"),
	ZSJY("01", "正扫交易"),
    FSJY("02", "反扫交易"),
    TKJY("06", "退款交易"),
    DTDDSM("08", "动态订单扫码"),
    APP_WAP("09", "APP/H5支付"),
    ;

    private final String code;
    private final String message;

    WsbankTradeTypeListEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (WsbankTradeTypeListEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static WsbankTradeTypeListEnum find(String code) {
        for (WsbankTradeTypeListEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<WsbankTradeTypeListEnum> getAll() {
        List<WsbankTradeTypeListEnum> list = new ArrayList<WsbankTradeTypeListEnum>();
        for (WsbankTradeTypeListEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (WsbankTradeTypeListEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
