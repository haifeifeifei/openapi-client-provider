package com.acooly.module.openapi.client.provider.wsbank.message.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/** @author zhike 2018/5/22 15:29 */
@Getter
@Setter
@XStreamAlias("head")
public class WsbankHeadResponse implements Serializable {
    /** 版本号 匹配接口文档版本。联调环境统一送1.0.0 */
    @XStreamAlias("Version")
    private String version = "1.0.0";

    /** 应用ID 由浙江网商银行统一分配,用于识别合作伙伴应用系统，即对端系统编号。联调前线下提供。注意此字段此处大小写要求。 */
    @XStreamAlias("Appid")
    @NotBlank
    private String appid;

    /** 接口代码 接口定义中的报文编号，明确接口功能，关联接口文档 */
    @XStreamAlias("Function")
    @NotBlank
    private String function;

    /** 报文发起时间 */
    @XStreamAlias("RespTime")
    @NotBlank
    private String respTime;

    /** 报文发起时区 应答发起系统所在时区 */
    @XStreamAlias("RespTimeZone")
    private String respTimeZone;

    /**
     *请求报文ID
     */
    @XStreamAlias("ReqMsgId")
    private String reqMsgId;

    /**
     * 报文字符编码
     */
    @XStreamAlias("InputCharset")
    private String inputCharset;

    /**
     * 保留字段
     * 使用KV方式表达
     */
    @XStreamAlias("Reserve")
    private String reserve;

    /**
     * 签名方式
     */
    @XStreamAlias("SignType")
    private String signType;
}
