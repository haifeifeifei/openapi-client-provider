package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author weili 2018/5/22 15:32
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankAddMerchantConfigResponseBody implements Serializable {

    /**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;

    /**
     *外部交易号
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;


}
