package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankBalancePayQueryRequestBody implements Serializable {

	/**
	 * 合作方机构号
	 */
    @Size(max = 32)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     * 付款方商户号
     */
    @Size(max = 32)
    @XStreamAlias("PayerMerchantId")
    @NotBlank
    private String payerMerchantId;

    /**
     * 收款方Id
     */
    @Size(max = 32)
    @XStreamAlias("PayeeId")
    @NotBlank
    private String payeeId;
    
    /**
     * 收款方类型
     */
    @Size(max = 32)
    @XStreamAlias("PayeeType")
    @NotBlank
    private String payeeType;

    /**
     * 外部订单请求流水号
     */
    @Size(max = 64)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;

    /**
     * 网商支付订单号
     */
    @Size(max = 64)
    @XStreamAlias("OrderNo")
    @NotBlank
    private String orderNo;
}
