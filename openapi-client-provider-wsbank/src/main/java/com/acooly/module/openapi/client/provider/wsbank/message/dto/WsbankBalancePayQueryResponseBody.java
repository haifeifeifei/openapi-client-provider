package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankBalancePayQueryResponseBody implements Serializable {

    /**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;

    /**
     * 外部交易号
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;

    /**
     * 网商支付订单号
     */
    @XStreamAlias("OrderNo")
    private String orderNo;
    
    /**
     * 付款方商户号
     */
    @XStreamAlias("PayerMerchantId")
    private String payerMerchantId;
    
	/**
	 * 收款方Id
	*/
	@XStreamAlias("PayeeId")
	private String payeeId;
	
	/**
	 * 收款方类型
	*/
	@XStreamAlias("PayeeType")
	private String payeeType;

    /**
     * 订单金额(金额为分)
     */
    @XStreamAlias("TotalAmount")
    private String totalAmount;

    /**
     * 币种，默认CNY
     */
    @XStreamAlias("Currency")
    private String currency;

    /**
     * 状态
     */
    @XStreamAlias("Status")
    private String status;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
