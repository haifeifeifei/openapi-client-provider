package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/24 14:51
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankOrdershareNotifyBody implements Serializable {

    /**
     * 商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
     */
    @XStreamAlias("MerchantId")
    private String merchantId;

    /**
     * 合作方机构号（网商银行分配）
     */
    @XStreamAlias("IsvOrgId")
    private String isvOrgId;

    /**
     * 关联网商订单号
     */
    @XStreamAlias("RelateOrderNo")
    private String relateOrderNo;

    /**
     * 外部交易号。由合作方系统生成。
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;

    /**
     * 分账单号
     */
    @XStreamAlias("ShareOrderNo")
    private String shareOrderNo;

    /**
     * 订单金额(金额为分)
     */
    @XStreamAlias("TotalAmount")
    private String totalAmount;

    /**
     * 币种，默认CNY
     */
    @XStreamAlias("Currency")
    private String currency;

    /**
     * 分账时间
     */
    @XStreamAlias("ShareDate")
    private String shareDate;

    /**
     * 状态(SUCCESS, FAIL)
     */
    @XStreamAlias("Status")
    private String status;

    /**
     * 错误码
     */
    @XStreamAlias("ErrorCode")
    private String errorCode;

    /**
     *错误描述
     */
    @XStreamAlias("ErrorDesc")
    private String errorDesc;

    /**
     *扩展信息
     */
    @XStreamAlias("ExtInfo")
    private String extInfo;

    /**
     * 备注
     */
    @XStreamAlias("Memo")
    private String memo;
}
