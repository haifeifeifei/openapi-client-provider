package com.acooly.module.openapi.client.provider.yibao.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoResponse;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/6/26 10:16
 */
@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_FIRSTPAY_REQUEST,type = ApiMessageType.Response)
public class YibaoFirstPayApplyResponse extends YibaoResponse {

    /**
     * 银行流水号
     */
    @YibaoAlias(value = "yborderid")
    private String bankOrderNo;

    /**
     * 计费商编
     */
    @Size(max = 32)
    @YibaoAlias(value = "csmerchantno")
    private String csMerchantNo;

    /**
     *手机号
     * 11 位数字
     */
    @NotBlank
    @YibaoAlias(value = "phone")
    private String mobileNo;

    /**
     * 实际短验发送方
     * YEEPAY：易宝
     * BANK：银行
     */
    @YibaoAlias(value = "codesender")
    private String codeSender;

    /**
     * 实际短验发送类型
     * VOICE：语音
     * MESSAGE：短信
     */
    @YibaoAlias(value = "smstype")
    private String smsType;

    /**
     * 订单状态
     * TO_VALIDATE：待短验确认
     * PAY_FAIL：支付失败
     * FAIL：系统异常
     * （FAIL 是非终态是异常状态，出现此状态建议
     * 查询）
     */
    @YibaoAlias(value = "status")
    private String status;
}
