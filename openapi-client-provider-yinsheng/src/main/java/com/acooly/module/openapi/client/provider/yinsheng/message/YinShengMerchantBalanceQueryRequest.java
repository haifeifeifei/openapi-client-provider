package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengRequest;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.YinShengMerchantBalanceQueryInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/2/22 9:58
 */
@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.MERCHANT_BALANCE_QUERY, type = ApiMessageType.Request)
public class YinShengMerchantBalanceQueryRequest extends YinShengRequest {

    /**
     * 提现申请业务信息
     */
    @ApiItem(value = "biz_content")
    private YinShengMerchantBalanceQueryInfo yinShengMerchantBalanceQueryInfo;
}
