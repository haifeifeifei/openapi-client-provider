package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayAlias;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayRequest;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/18 11:38
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.REALTIME_LOCATION_VERIFY_POSITION,type = ApiMessageType.Request)
public class YipayRealTimeLocationVerifyPositionRequest extends YipayRequest {

    /**
     * 手机号码
     * 必须是11位（电信或联通）手机号码
     */
    @NotBlank
    @Size(min = 11,max = 11)
    @YipayAlias(value = "mobile")
    private String mobile;

    /**
     * 纬度
     */
    @NotBlank
    @Size(min = 1,max = 32)
    @YipayAlias(value = "latitude")
    private String latitude;

    /**
     * 经度
     */
    @NotBlank
    @Size(min = 1,max = 32)
    @YipayAlias(value = "longitude")
    private String longitude;
}
