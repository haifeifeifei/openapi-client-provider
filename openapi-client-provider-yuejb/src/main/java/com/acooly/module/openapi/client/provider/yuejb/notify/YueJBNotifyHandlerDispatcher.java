/**
 * coding by zhangpu
 */
package com.acooly.module.openapi.client.provider.yuejb.notify;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;

import java.util.Map;

/**
 * yuejb支付网关异步通知分发器
 * Created by ouwen@yiji.com} on 2017/11/6.
 */
public class YueJBNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return Strings.substringAfterLast(notifyUrl, "/");
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return null;
    }
}
