package com.acooly.openapi.client.provider.baofu;

import com.acooly.module.openapi.client.provider.baofu.message.*;
import com.google.common.collect.Lists;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.baofu.BaoFuApiService;
import com.acooly.module.openapi.client.provider.baofu.BaoFuConstants;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuBatchDeductRequest;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuBatchDeductResponse;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuBatchDeductSigleQueryRequest;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuBatchDeductSigleQueryResponse;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuBillDowloadRequest;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuBillDowloadResponse;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuDeductRequest;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuDeductResponse;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuRefundQueryRequest;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuRefundQueryResponse;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuRefundRequest;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuRefundResponse;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuTradeQueryRequest;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuTradeQueryResponse;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuWithdrawQueryRequest;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuWithdrawQueryResponse;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuWithdrawRequest;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuWithdrawResponse;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuBatchDeductInfo;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuWithdrawQueryRequestInfo;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuWithdrawRequestInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @author zhike 2018/1/26 18:03
 */
@SpringBootApplication
@BootApp(sysName = "test")
public class BaofuTest extends NoWebTestBase {

    @Autowired
    private BaoFuApiService baoFuApiService;

    /**
     * 测试代扣
     */
    @Test
    public void testDeduct() {
        BaoFuDeductRequest request = new BaoFuDeductRequest();
        request.setGatewayUrl(BaoFuConstants.BAOFU_SINGLE_DEDUCT_URL);
        request.setPayCode("ABC");
        request.setPayCm("1");
        request.setAccNo("6228480402564890018");
        request.setIdCard("500221198810192313");
        request.setIdHolder("志客");
        request.setMobile("18695625227");
        request.setValidDate("2019");
        request.setValidNo("123");
        request.setTransId(Ids.oid());
        request.setTxnAmt("100");
        request.setTradeDate(Dates.format(new Date(),"yyyyMMddHHmmss"));
        BaoFuDeductResponse response = baoFuApiService.deduct(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试代扣查询20180201182405
     */
    @Test
    public void testDeductQuery() {
        BaoFuTradeQueryRequest request = new BaoFuTradeQueryRequest();
        request.setGatewayUrl(BaoFuConstants.BAOFU_SINGLE_DEDUCT_URL);
        request.setOrigTransId("o18020118240569570002");
        request.setOrigTradeDate("20180131103604");
        BaoFuTradeQueryResponse response = baoFuApiService.tradeQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试批量代扣
     */
    @Test
    public void testBatchDeduct() {
        BaoFuBatchDeductRequest request = new BaoFuBatchDeductRequest();
        request.setTransBatchId(Ids.oid());
        request.setGatewayUrl(BaoFuConstants.BAOFU_BATCH_DEDUCT_URL);
        List<BaoFuBatchDeductInfo> baoFuBatchDeductInfos = Lists.newArrayList();
        IntStream.range(0, 20).mapToObj(i -> new BaoFuBatchDeductInfo()).forEach(baoFuBatchDeductInfo -> {
            baoFuBatchDeductInfo.setAccNo("6228480402564890018");
            baoFuBatchDeductInfo.setIdCardType("01");
            baoFuBatchDeductInfo.setIdCard("500221198810192313");
            baoFuBatchDeductInfo.setIdHolder("志客");
            baoFuBatchDeductInfo.setMobile("18695625227");
            baoFuBatchDeductInfo.setTransId(Ids.oid());
            baoFuBatchDeductInfo.setTxnAmt("100");
            baoFuBatchDeductInfo.setTradeDate(Dates.format(new Date(), "yyyyMMddHHmmss"));
            baoFuBatchDeductInfos.add(baoFuBatchDeductInfo);
        });
        request.setBaoFuBatchDeductInfos(baoFuBatchDeductInfos);
        BaoFuBatchDeductResponse response = baoFuApiService.batchDeduct(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试批量代扣单笔查询
     */
    @Test
    public void testBatchDeductSigleQuery() {
        BaoFuBatchDeductSigleQueryRequest request = new BaoFuBatchDeductSigleQueryRequest();
        request.setGatewayUrl(BaoFuConstants.BAOFU_BATCH_DEDUCT_URL);
        request.setTransId("o18020114021684490022");
        BaoFuBatchDeductSigleQueryResponse response = baoFuApiService.batchDeductSigleQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试提现
     */
    @Test
    public void testWithdraw() {
        BaoFuWithdrawRequest request = new BaoFuWithdrawRequest();
        request.setGatewayUrl(BaoFuConstants.BAOFU_WITHDRAW_URL);
        List<BaoFuWithdrawRequestInfo> withdrawInfos = Lists.newArrayList();
        BaoFuWithdrawRequestInfo withdrawRequestInfo = new BaoFuWithdrawRequestInfo();
        withdrawRequestInfo.setTransNo(Ids.oid());
        withdrawRequestInfo.setTransMoney("3");
        withdrawRequestInfo.setToAccName("张三");
        withdrawRequestInfo.setToAccNo("6222601234567890");
        withdrawRequestInfo.setToBankName("中国工商银行");
        withdrawRequestInfo.setToProName("上海市");
        withdrawRequestInfo.setToCityName("上海市");
        withdrawRequestInfo.setToAccDept("");
        withdrawInfos.add(withdrawRequestInfo);
        request.setWithdrawInfos(withdrawInfos);
        BaoFuWithdrawResponse response = baoFuApiService.withdraw(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试提现查询
     */
    @Test
    public void testWithdrawQuery() {
        BaoFuWithdrawQueryRequest request = new BaoFuWithdrawQueryRequest();
        request.setGatewayUrl(BaoFuConstants.BAOFU_WITHDRAW_QUERY_URL);
        List<BaoFuWithdrawQueryRequestInfo> withdrawQueryInfos = Lists.newArrayList();
        BaoFuWithdrawQueryRequestInfo withdrawQueryRequestInfo = new BaoFuWithdrawQueryRequestInfo();
        withdrawQueryRequestInfo.setTransNo("o18020510383415220001");
        withdrawQueryInfos.add(withdrawQueryRequestInfo);
        request.setWithdrawQueryInfos(withdrawQueryInfos);
        BaoFuWithdrawQueryResponse response = baoFuApiService.withdrawQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试代扣查询
     */
    @Test
    public void testBillDownload() {
        BaoFuBillDowloadRequest request = new BaoFuBillDowloadRequest();
        request.setGatewayUrl(BaoFuConstants.BAOFU_DOWNLOAD_BILL_URL);
        request.setFileType("fi");
        request.setClientIp("113.250.252.77");
        request.setSettleDate("2018-01-20");
//        request.setFilePath("D:/var");
        request.setFileName("aa");
        request.setFileSuffix("zip");
        BaoFuBillDowloadResponse response = baoFuApiService.billDownload(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }


    /**
     * 退款API
     */
    @Test
    public void testRefund() {
        BaoFuRefundRequest request = new BaoFuRefundRequest();
        request.setGatewayUrl("https://public.baofoo.com/cutpayment/api/backTransRequest");
        request.setTransId(Ids.oid());
        request.setRefundType("2");
        request.setRefundOrderNo(Ids.oid());
        request.setTransSerialNo(Ids.oid());
        request.setRefundReason("我就想退");
        request.setRefundAmt("100");
        request.setRefundTime(Dates.format(new Date(),"yyyyMMddHHmmss"));
        BaoFuRefundResponse response = baoFuApiService.refund(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试退款查询
     */
    @Test
    public void testRefundQuery() {
        BaoFuRefundQueryRequest request = new BaoFuRefundQueryRequest();
        request.setGatewayUrl(BaoFuConstants.BAOFU_REFUND);
        request.setRefundOrderNo(Ids.oid());
        request.setTransSerialNo(Ids.oid());
        BaoFuRefundQueryResponse response = baoFuApiService.refundQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试商户账户余额查询接口
     */
    @Test
    public void testAccountBalanceQuery() {
        BaoFuAccountBalanceQueryRequest request = new BaoFuAccountBalanceQueryRequest();
        request.setAccountType("0");
        request.setGatewayUrl("https://public.baofoo.com/open-service/query/service.do");
        BaoFuAccountBalanceQueryResponse response = baoFuApiService.accountBalanceQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }
}
