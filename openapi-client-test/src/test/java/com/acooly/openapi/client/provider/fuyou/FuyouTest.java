package com.acooly.openapi.client.provider.fuyou;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.fuyou.FuyouApiService;
import com.acooly.module.openapi.client.provider.fuyou.message.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhike 2018/3/19 16:11
 */
@SpringBootApplication
@BootApp(sysName = "test")
public class FuyouTest extends NoWebTestBase {

    @Autowired
    private FuyouApiService fuyouApiService;

    /**
     * 3.1 测试首次协议下单，验证码获取接口 （通）
     */
    @Test
    public void testFirstQuickPayCreateOrder() {
        FuyouFirstQuickCreateOrderRequest request = new FuyouFirstQuickCreateOrderRequest();
        request.setAmount("10");
        request.setBankCard("6228480402564891128");
        request.setIdNumber("150206197505017881");
        request.setMerchOrderNo(Ids.oid());
        request.setMobileNo("18696725231");
        request.setOrderAliveTime("20");
        request.setReaNname("计尔琴");
        request.setUserIp("192.168.55.158");
        request.setUserId(Ids.oid());
        FuyouFirstQuickCreateOrderResponse response = fuyouApiService.quickCreateOrder(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 3.2 测试首次协议支付接口（通）
     */
    @Test
    public void testFirstAgreementPay(){
        FuyouFirstAgreementPayRequest request = new FuyouFirstAgreementPayRequest();
        request.setBankCard("6228480402564891128");
        request.setMerchOrderNo("o18032812070739100001");
        request.setMobileNo("18696725231");
        request.setVercd("000000");
        request.setUserIp("192.168.55.158");
        request.setUserId("o18032817153076680002");
        request.setOrderId("000035211675");
        request.setSignPay("api,689d325333e5e86ee041ef10e1b487a5,nawh,9,4207");
        FuyouFirstAgreementPayResponse response = fuyouApiService.firstAgreementPay(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }


    /**
     * 3.3 商户首次协议下单重发验证码接口（通）
     */
    @Test
    public void testFirstAgrpayRetCodeOrder() {
        FuyouFirstAgrpayRetCodeOrderRequest request = new FuyouFirstAgrpayRetCodeOrderRequest();
        request.setUserIp("192.168.55.158");
        request.setOrderId("000035175694");
        request.setUserId("o18032117521616440002");
        request.setAmount("10");
        request.setBankCard("6228480402564891129");
        request.setReaNname("志客");
        //request.setIdType("0");
        request.setIdNumber("500221198810192313");
        request.setMobileNo("18696725230");

        FuyouFirstAgrpayRetCodeOrderResponse response = fuyouApiService.firstAgrpayRetCodeOrder(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 3.4 协议下单验证码获取（NO）
     */
    @Test
    public void testAgrpayGetCodeOrder() {

        FuyouAgrpayGetCodeOrderRequest request = new FuyouAgrpayGetCodeOrderRequest();
        request.setUserIp("192.168.55.158");
        request.setMerchOrderNo(Ids.oid());
        request.setUserId("o18032117382032200002");
        request.setAmount("1");
        request.setProtocolNo("15216247302648553237");
        FuyouAgrpayGetCodeOrderResponse response = fuyouApiService.agrpayGetCodeOrder(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }


    /**
     * 3.5 协议下单重发验证码接口（NO）
     */
    @Test
    public void testAgrpayRetCodeOrder() {
        FuyouAgrpayRetCodeOrderRequest request = new FuyouAgrpayRetCodeOrderRequest();
        request.setUserIp("192.168.55.158");
        request.setUserId("o18032117382032200002");
        request.setBankOrderId("000035176174");
        request.setAmount("1");
        request.setProtocolNo("15216247302648553237");
        FuyouAgrpayRetCodeOrderResponse response = fuyouApiService.agrpayRetCodeOrder(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 3.6协议支付（NO）
     */
    @Test
    public void testAgreementPay() {
        FuyouAgreementPayRequest request = new FuyouAgreementPayRequest();
        request.setUserIp("192.168.55.158");
        request.setMerchOrderNo(Ids.oid());
        request.setUserId(Ids.oid());
        request.setBankOrderId("000035157878");
        request.setProtocolNo("14907763938986631634");
        request.setVercd("123456");
        request.setSignPay("123456");
        FuyouAgreementPayResponse response = fuyouApiService.agreementPay(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 3.7 协议解绑（过）
     */
    @Test
    public void testAgreementUnbundling() {
        FuyouAgreementUnbundlingRequest request = new FuyouAgreementUnbundlingRequest();
        request.setUserId(Ids.oid());
        request.setProtocolNo("14907763938986631634");
        FuyouAgreementUnbundlingResponse response = fuyouApiService.agreementUnbundling(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 3.8协议卡查询（过）
     */
    @Test
    public void testAgreementCardQuery() {
        FuyouAgreementCardQueryRequest request = new FuyouAgreementCardQueryRequest();
        request.setUserId("o18032016164888960002");
        FuyouAgreementCardQueryResponse response = fuyouApiService.agreementCardQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 3.9订单结果查询（富友订单号）(失败：服务端异常)
     */
    @Test
    public void testFuyouOrderResultQuery() {
        FuyouOrderResultQueryRequest request = new FuyouOrderResultQueryRequest();
        request.setMerchOrderNo("o18032016164808960001");
        FuyouOrderResultQueryResponse response = fuyouApiService.fuyouOrderResultQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 3.10  订单结果查询接口（商户订单号）（过）
     */
    @Test
    public void testMerOrderResultQuery() {
        FuyouMerOrderResultQueryRequest request = new FuyouMerOrderResultQueryRequest();
        request.setMerchOrderNo("o18032016164808960001");
        FuyouMerOrderResultQueryResponse response = fuyouApiService.merOrderResultQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 3.11  未支付订单关闭接口 （过）
     */
    @Test
    public void testUnpaidOrderClosed() {
        FuyouUnpaidOrderClosedRequest request = new FuyouUnpaidOrderClosedRequest();
        request.setBankOrderId("000035157878");
        request.setAmount("10");
        request.setMerchOrderNo("6164808960001");
        FuyouUnpaidOrderClosedResponse response = fuyouApiService.unpaidOrderClosed(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }


    /**
     * 3.12商户支持卡Bin查询接口（失败：服务端异常:null）
     */
    @Test
    public void testMerSupportCardBinQuery(){
        FuyouMerSupportCardBinQueryRequest request = new FuyouMerSupportCardBinQueryRequest();
        request.setONo("6228480402564891119");
        FuyouMerSupportCardBinQueryResponse response = fuyouApiService.merSupportCardBinQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 3.13限额查询（过）
     */
    @Test
    public void testQuotaQuery(){
        FuyouQuotaQueryRequest request = new FuyouQuotaQueryRequest();
        request.setInsCd("0803090000");
        FuyouQuotaQueryResponse response = fuyouApiService.quotaQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试网关充值（过）
     */
    @Test
    public void testNetBankDeposit(){
        FuyouNetBankRequest request = new FuyouNetBankRequest();
        request.setOrderId(Ids.oid());
        request.setOrderAmt("100");
        request.setOrderPayType("B2C");
        request.setPageNotifyUrl("http://218.70.106.250:8881/gateway/redirect/fuyou/netbankReturn");
//        request.setOrderValidTime("10m");
        request.setIssInsCd("0803030000");
        String response = fuyouApiService.netBankDeposit(request);
        System.out.println("响应实体报文response=" + response);
    }

    /**
     * 测试网关充值（过）
     */
    @Test
    public void testNetBankDepositQuery(){
        FuyouNetBankSynchroQueryRequest request = new FuyouNetBankSynchroQueryRequest();
        request.setOrderId("18032816541892600028");
        FuyouNetBankSynchroQueryResponse response = fuyouApiService.netBankDepositQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试提现
     */
    @Test
    public void testWithdraw(){
        FuyouWithdrawRequest request = new FuyouWithdrawRequest();
        request.setBankNo("0103");
        request.setCityNo("1000");
        request.setAccntNo("6228480402564890018");
        request.setAccntNm("志客");
        request.setAmt("4567");
        FuyouWithdrawResponse response = fuyouApiService.withdraw(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试提现订单查询
     */
    @Test
    public void testWithdrawQuery(){
        FuyouWithdrawQueryRequest request = new FuyouWithdrawQueryRequest();
        request.setOrderNo("o18032709382287100001");
        request.setStartDt("20180325");
        request.setEndDt("20180327");
        FuyouWithdrawQueryResponse response = fuyouApiService.withdrawQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 协议卡绑定发送短信验证码1.3
     */
    @Test
    public void testBindMsg() {
        FuyouBindMsgRequest request = new FuyouBindMsgRequest();
        request.setBankCardNo("6228480402564891128");
        request.setIdCardNo("150206197505017881");
        request.setOrderNo(Ids.oid());
        request.setMobileNo("18696725231");
        request.setReaNname("计尔琴");
        request.setUserId(Ids.oid());
        FuyouBindMsgResponse response = fuyouApiService.bindMsg(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 协议卡绑定发送短信验证码1.3
     */
    @Test
    public void testBindCommit() {
        FuyouBindCommitRequest request = new FuyouBindCommitRequest();
        request.setBankCardNo("6228480402564891128");
        request.setIdCardNo("150206197505017881");
        request.setOrderNo("o18060411552863560001");
        request.setMobileNo("18696725231");
        request.setReaNname("计尔琴");
        request.setUserId("o18060411552821560002");
        request.setSmsCode("000000");
        FuyouBindCommitResponse response = fuyouApiService.bindCommit(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 协议解绑1.3
     */
    @Test
    public void testUnBind() {
        FuyouUnBindRequest request = new FuyouUnBindRequest();
        request.setUserId("o18060411552821560002");
        request.setProtocolNo("KMXA18100000000879MCYD");
        FuyouUnBindResponse response = fuyouApiService.unBind(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 协议卡查询1.3
     */
    @Test
    public void testBindQuery() {
        FuyouBindQueryRequest request = new FuyouBindQueryRequest();
        request.setUserId("o18060411552821560002");
        FuyouBindQueryResponse response = fuyouApiService.bindQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }


    /**
     * 协议卡支付1.3
     */
    @Test
    public void testOrderPay() {
        FuyouOrderPayRequest request = new FuyouOrderPayRequest();
        request.setUserIp("192.168.55.158");
        request.setMerchOrderNo(Ids.oid());
        request.setUserId("o18060411552821560002");
        request.setAmount("1");
        request.setProtocolNo("KMXA18100000000879MCYD");
//        request.setProtocolNo("KMXA18100000000879MCYs");
        FuyouOrderPayResponse response = fuyouApiService.orderPay(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

}