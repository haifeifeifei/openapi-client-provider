package com.acooly.openapi.client.provider.jyt;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.jyt.JytApiService;
import com.acooly.module.openapi.client.provider.jyt.domain.JytRequest;
import com.acooly.module.openapi.client.provider.jyt.message.*;
import com.acooly.module.openapi.client.provider.jyt.message.dto.*;
import com.acooly.module.openapi.client.provider.yinsheng.YinShengApiService;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengAppPayBankTypeEnum;
import com.acooly.module.openapi.client.provider.yinsheng.message.*;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.*;
import com.acooly.module.openapi.client.provider.yinsheng.message.netbank.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
@BootApp(sysName = "jytTest")
public class JytTest extends NoWebTestBase {
    @Autowired
    private JytApiService jytApiService;

    /**
     * 测试代扣申请接口
     */
    @Test
    public void deductApply() {
        JytDeductApplyRequest request = new JytDeductApplyRequest();
        JytDeductApplyRequestBody requestBody = new JytDeductApplyRequestBody();
        requestBody.setUserId("o18050819384886600001");
        requestBody.setOrderId(Ids.oid());
        requestBody.setAmount("1");
        requestBody.setBankCardNo("6217002710000684875");
        requestBody.setIdCardNo("500221198810192313");
        requestBody.setMobileNo("18696725229");
        requestBody.setRealName("李四");
        request.setDeductApplyRequestBody(requestBody);
        JytDeductApplyResponse response = jytApiService.deductApply(request);
        System.out.println("代扣申请接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试代扣支付接口
     */
    @Test
    public void deductPay() {
        JytDeductPayRequest request = new JytDeductPayRequest();
        JytDeductPayRequestBody requestBody = new JytDeductPayRequestBody();
        requestBody.setOrderId("o18050913481688600001");
        requestBody.setSmsCode("724099");
        request.setDeductPayRequestBody(requestBody);
        JytDeductPayResponse response = jytApiService.deductPay(request);
        System.out.println("代扣支付接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试重新获取验证码接口
     */
    @Test
    public void retrievesSmsCode() {
        JytRetrievesSmsCodeRequest request = new JytRetrievesSmsCodeRequest();
        JytRetrievesSmsCodeRequestBody requestBody = new JytRetrievesSmsCodeRequestBody();
        requestBody.setOrderId("o18050913364279840001");
        requestBody.setMobileNo("18696725229");
        request.setRetrievesSmsCodeRequestBody(requestBody);
        JytRetrievesSmsCodeResponse response = jytApiService.retrievesSmsCode(request);
        System.out.println("重新获取验证码接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试支付交易单笔查询接口
     */
    @Test
    public void tradeOrderQuery() {
        JytTradeOrderQueryRequest request = new JytTradeOrderQueryRequest();
        JytTradeOrderQueryRequestBody requestBody = new JytTradeOrderQueryRequestBody();
        requestBody.setOrderId("o18050913364279840001");
        request.setTradeOrderQueryRequestBody(requestBody);
        JytTradeOrderQueryResponse response = jytApiService.tradeOrderQuery(request);
        System.out.println("支付交易单笔查询接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试四要素验卡发送短息
     */
    @Test
    public void checkBankCardApply() {
        JytCheckCardApplyRequest request = new JytCheckCardApplyRequest();
        JytCheckCardApplyRequestBody requestBody = new JytCheckCardApplyRequestBody();
        requestBody.setOrderId(Ids.oid());
        requestBody.setUserId(Ids.oid());
        requestBody.setBankCardNo("6217002710000684871");
        requestBody.setIdCardNo("500221198810192313");
        requestBody.setMobileNo("18696725229");
        requestBody.setRealName("志客");
        request.setCheckCardApplyRequestBody(requestBody);
        JytCheckCardApplyResponse response = jytApiService.checkCardApply(request);
        System.out.println("测试四要素验卡发送短息接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试四要输验卡确认
     */
    @Test
    public void checkBankCardConfirm() {
        JytCheckCardConfirmRequest request = new JytCheckCardConfirmRequest();
        JytCheckCardConfirmRequestBody requestBody = new JytCheckCardConfirmRequestBody();
        requestBody.setBindOrderNo("o18061512382703280001");
        requestBody.setSmsCode("566750");
        request.setCheckCardConfirmRequestBody(requestBody);
        JytCheckCardConfirmResponse response = jytApiService.checkCardConfirm(request);
        System.out.println("支付交易单笔查询接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试解除实名支付银行卡
     */
    @Test
    public void unBindCard() {
        JytUnBindCardRequest request = new JytUnBindCardRequest();
        JytUnBindCardRequestBody requestBody = new JytUnBindCardRequestBody();
        requestBody.setUserId("o18061512382703280001");
        requestBody.setBankCardNo("6217002710000684871");
        request.setUnBindCardRequestBody(requestBody);
        JytUnBindCardResponse response = jytApiService.unBindCard(request);
        System.out.println("解除实名支付银行卡接口响应报文："+ JSON.toJSONString(response));
    }
}
