package com.acooly.openapi.client.provider.wsbank.alipayrequest;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankAlipayRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankAlipayResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankAlipayRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankAlipayRequestInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "wsbankAlipayrequestTest")
public class WsbankAlipayrequestTest extends NoWebTestBase {
    @Autowired
    private WsbankApiService wsbankApiService;
    
    /**
     * 测试支付宝APP/WAP支付接口
     */
    public void alipayRequestForPayTypeApp() {
    	WsbankAlipayRequestBody requestBody = new WsbankAlipayRequestBody();
    	requestBody.setPayType("app");
    	requestBody.setBody("测试商品");
    	requestBody.setSubject("大乐透");
        requestBody.setOutTradeNo(Ids.oid());
        requestBody.setTotalAmount("10000");
        requestBody.setMerchantId("226801000000103460124");
        WsbankAlipayRequestInfo requestInfo = new WsbankAlipayRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankAlipayRequest request = new WsbankAlipayRequest();
        request.setRequestInfo(requestInfo);
        WsbankAlipayResponse response = wsbankApiService.alipayRequest(request);
        System.out.println("支付宝APP/WAP支付接口："+ JSON.toJSONString(response));
    }
    
    /**
     * 测试支付宝APP/WAP支付接口
     */
    @Test
    public void alipayRequestForPayTypeWap() {
    	WsbankAlipayRequestBody requestBody = new WsbankAlipayRequestBody();
    	requestBody.setPayType("wap");
    	requestBody.setBody("测试商品");
    	requestBody.setSubject("大乐透");
        requestBody.setOutTradeNo(Ids.oid());
        requestBody.setTotalAmount("10000");
        requestBody.setMerchantId("226801000000103460124");
        WsbankAlipayRequestInfo requestInfo = new WsbankAlipayRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankAlipayRequest request = new WsbankAlipayRequest();
        request.setRequestInfo(requestInfo);
        WsbankAlipayResponse response = wsbankApiService.alipayRequest(request);
        System.out.println("支付宝APP/WAP支付接口："+ JSON.toJSONString(response));
    }
}
